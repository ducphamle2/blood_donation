// import App from 'next/app'
import HelloWorld from './HelloWorld'

import React, { Component } from "react";
import {Button} from "antd";
import Router from "next/router";

export default class App extends Component {

  routeHelloWorld = () => {
      Router.push("/HelloWorld").then(() => {
          //maybe dispatch state loaded page HelloWorld...
      });
  };

  render() {
    return (
        <div style={{margin: '16px'}}>
          <h2>This is the homepage</h2>
          <Button onClick={this.routeHelloWorld}>HelloWorld</Button>
        </div>

    );
  }
}
