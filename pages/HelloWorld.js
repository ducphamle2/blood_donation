import React from 'react';
import { Layout } from 'antd';
import HeaderLayout from "../src/components/HeaderLayout";
import {USER_TYPE} from "../config/config.js";
import { connect } from 'react-redux'
import LoginAction from '../src/redux/actions/LoginAction';

class HomePage extends React.Component {
    constructor(props) { // must have a constructor to inherit props from parent component
        super(props);
        // bind all the props and state to this function
        this.state = {
            userName: "",
            userType: ""
        }
    }

    testFunction = (userType) => {
        const { dispatch, userName } = this.props; // get dispatch prop and data from redux
        // here userName is collected from the state.Reducer.userName
        let payload = {
            userName: "ABCD",
            userType: userType
        }
        // demo dispatch to change data from redux.
        // to trigger component change, use it with state
        if (userType != USER_TYPE.NONE) dispatch(LoginAction.userLogIn(payload));
        else dispatch(LoginAction.userLogOut());
        // for example, we use state to re-render the component, as dispatch wont trigger immmediately. We can try using componentWillReceiveNewProps for the current component, but i have not tried yet, maybe you can test it.
        // however, other screens will receive change, use dispatch and move to other screens to see changes.
        this.setState({
            userName: "ABCD",
            userType: "Hospital"
        })
    }

    render() {
        return (
            <Layout>
                {/*<HeaderLayout userType={accountKey} setAccount={setAccount}></HeaderLayout>*/}
                <HeaderLayout setAccount={this.testFunction} />
            </Layout>
        );
    }
}

export default connect(state => ({
    userName: state.LoginReducer.userName,
    userType: state.LoginReducer.userType
}))(HomePage);