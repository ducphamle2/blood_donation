import React, {Component} from "react";
import {Provider} from "react-redux";
import store from "../src/redux/store";
import 'antd/dist/antd.css';

export default function App({Component, pageProps}) {
    return (
        <Provider store={store}>
            <Component {...pageProps} />
        </Provider>
    );
}