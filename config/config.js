export const USER_TYPE = {
    NONE: 0,
    DONOR: 1,
    HOSPITAL: 2,
    ORGANIZER: 3,
    RED_CROSS: 4
}