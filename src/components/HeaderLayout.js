import { Layout, Menu, Row, Col, Breadcrumb } from "antd";
import React from 'react';
import { USER_TYPE } from "../../config/config";
import { connect } from 'react-redux'


class HeaderLayout extends React.Component {
    //props: userName, userType, setAccount

    constructor(props) {
        super(props)
    }

    handleMenuItemClick = (e) => {
        this.props.setAccount(e.key);
    };

    render() {

        return (
            <Layout>
                <Layout style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
                    <Menu theme="light" mode="horizontal" defaultSelectedKeys={['2']}
                        onSelect={this.handleMenuItemClick}
                    >
                        <Menu.Item key={USER_TYPE.RED_CROSS}>Red Cross</Menu.Item>
                        <Menu.Item key={USER_TYPE.DONOR}>Donor</Menu.Item>
                        <Menu.Item key={USER_TYPE.HOSPITAL}>Hospital</Menu.Item>
                        <Menu.Item key={USER_TYPE.ORGANIZER}>Organizer</Menu.Item>
                        {
                            this.props.userType ? <Menu.Item key={USER_TYPE.NONE}>Log Out</Menu.Item> : null
                        }
                    </Menu>
                </Layout>
                <Layout style={{ padding: '48px' }}>
                    <div style={{ 'backgroundColor': '#ffffff', 'minHeight': 'calc(100vh)', 'marginTop': '48px' }}>
                        {this.props.userType ? "User type: " + this.props.userType : "Sign in required"}
                    </div>
                </Layout>
            </Layout>
        )
    }
}

export default connect(state => ({
    userName: state.LoginReducer.userName,
    userType: state.LoginReducer.userType
}))(HeaderLayout);