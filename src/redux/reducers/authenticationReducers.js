/**
 * @author DucPL
 */

import ActionType from "../ActionTypes";

// a place to store common variables, and we change these vars by calling different Action functions.
const LoginReducer = (
    state = {
        userName: "",
        userType: ""
    },
    action
) => {
    const {type, payload} = action;
    switch (type) {
        case ActionType.LOGIN:
            return {
                ...state,
                userName: payload.userName,
                userType: payload.userType
            };

        case ActionType.LOGOUT:
            return {
                ...state,
                userName: "",
                userType: ""
            };
        default:
            return state;
    }
};

export default LoginReducer;
