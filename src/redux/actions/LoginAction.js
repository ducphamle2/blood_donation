import ActionType from '../ActionTypes';

// an object which contains different functions that pass type and payload params for reducers
const LoginAction = {
    userLogIn: payload => ({ type: ActionType.LOGIN, payload }),
    userLogOut: () => ({ type: ActionType.LOGOUT }),
}

export default LoginAction;